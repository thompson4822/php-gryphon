<?php

class Rental {
  public $id = -1;
  public $inventoryItemId = -1;
  public $personId = -1;
  public $rentalDate = "";
  public $dueDate = "";
  public $returnDate = "";
  public $creditAmount = 0.0;
  public $tax = 0.0;
  public $subtotal = 0.0;

  public function __construct() {
    
  }
}

