<?php

class ProductReturn {
  public $storeId = -1;
  public $saleId = -1;
  public $inventoryItemId = -1;
  public $reason = "";
  public $condition = ConditionType::NA;
  public $refundAmount = 0.0;
  public $returnData = "";

  public function __construct() {
    
  }
}

