<?php

abstract class PaymentType {
  const None = 0;
  const Cash = 1;
  const CreditCard = 2;
  const Cheque = 3;
}

