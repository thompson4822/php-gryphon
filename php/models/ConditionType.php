<?php

abstract class ConditionType {
  const NA = 0;
  const Damaged = 1;
  const SlightWear = 2;
  const Good = 3;
  const LikeNew = 4;
}

