<?php

abstract class CardType {
  const None = 0;
  const Visa = 1;
  const MasterCard = 2;
  const AmericanExpress = 3; 
}

