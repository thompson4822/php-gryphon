<?php

class Address {
  public $id = -1;                 // Primary key
  public $addressLine1 = "";            // Street address1 (ie: 1451 Draper Way)
  public $addressLine2 = "";    // Street address2 (ie: P.O. Box 101)
  public $city = "";                    // City
  public $state = "";                   // State
  public $postalCode = "";

  public function __construct() {

  }
}

class User {

  public function __construct() {
    
  }
}

