CREATE DATABASE IF NOT EXISTS Gryphon;

USE Gryphon;

CREATE TABLE IF NOT EXISTS `Address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,             
  `addressLine1` varchar(200),
  `addressLine2` varchar(200),
  `city` varchar(100),
  `state` varchar(50),
  `postalCode` varchar(15),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `Credit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Credit` 
ADD CONSTRAINT FK_credit 
FOREIGN KEY (`personId`) REFERENCES `Person`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS `CreditCard` (
  `cardType` tinyint(2) NOT NULL,
  `cardNumber` varchar(20)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `CreditCardInfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personId` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `expiration` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `CreditCardInfo` 
ADD CONSTRAINT FK_creditCardInfo 
FOREIGN KEY (`personId`) REFERENCES `Person`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `Employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `ssn` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `roleId` varchar(20) NOT NULL,
  `hireDate` varchar(20) NOT NULL,
  `exitDate` varchar(20),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Employee` 
ADD CONSTRAINT FK_employeePerson 
FOREIGN KEY (`personId`) REFERENCES `Person`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `Employee` 
ADD CONSTRAINT FK_employeeStore 
FOREIGN KEY (`storeId`) REFERENCES `Store`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `InventoryItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeId` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `baseCost` decimal(10,2) NOT NULL,
  `barcode` varchar(120) NOT NULL,
  `tracked` varchar(20),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `InventoryItem` 
ADD CONSTRAINT FK_inventoryItemStore 
FOREIGN KEY (`storeId`) REFERENCES `Store`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `InventoryItemType` (
  `inventoryItemId` int(11) NOT NULL,
  `inventoryTypeId` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `InventoryItemType` 
ADD CONSTRAINT FK_inventoryItemTypeItem 
FOREIGN KEY (`inventoryItemId`) REFERENCES `InventoryItem`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `InventoryItemType` 
ADD CONSTRAINT FK_inventoryItemTypeTypeId 
FOREIGN KEY (`inventoryTypeId`) REFERENCES `InventoryType`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `InventoryTransfer` (
  `inventoryItemId` int(11) NOT NULL,
  `storeFromId` int(11) NOT NULL,
  `storeToId` int(11) NOT NULL,
  `transferDate` varchar(20)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `InventoryTransfer` 
ADD CONSTRAINT FK_inventoryTransferItem
FOREIGN KEY (`inventoryItemId`) REFERENCES `InventoryItem`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `InventoryTransfer` 
ADD CONSTRAINT FK_inventoryTransferStoreFrom
FOREIGN KEY (`storeFromId`) REFERENCES `Store`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `InventoryTransfer` 
ADD CONSTRAINT FK_inventoryTransferStoreTo
FOREIGN KEY (`storeToId`) REFERENCES `Store`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `InventoryType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(30),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `LateFee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rentalId` int(11) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `LateFee` 
ADD CONSTRAINT FK_lateFeeRental
FOREIGN KEY (`rentalId`) REFERENCES `Rental`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `Payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentType` tinyint(2) NOT NULL,
  `creditCardInfoId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `datePaid` varchar(20),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Payment` 
ADD CONSTRAINT FK_paymentCreditCardInfo
FOREIGN KEY (`creditCardInfoId`) REFERENCES `CreditCardInfo`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `Person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(20),
  `lastName` varchar(20),
  `addressId` int(11) NOT NULL,
  `primaryPhone` varchar(20),
  `email` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Person` 
ADD CONSTRAINT FK_personAddress
FOREIGN KEY (`addressId`) REFERENCES `Address`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `Rental` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventoryItemId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `rentalDate` varchar(20),
  `dueDate` varchar(20),
  `returnDate` varchar(20),
  `creditAmount` decimal(10,2),
  `tax` decimal(10,2) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Rental` 
ADD CONSTRAINT FK_rentalInventoryItem
FOREIGN KEY (`inventoryItemId`) REFERENCES `InventoryItem`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `RentalFeedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rentalId` int(11) NOT NULL,
  `rating` tinyint(2) NOT NULL,
  `other` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `RentalFeedback` 
ADD CONSTRAINT FK_rentalFeedbackRental
FOREIGN KEY (`rentalId`) REFERENCES `Rental`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;



CREATE TABLE IF NOT EXISTS `RentalPayment` (
  `rentalId` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `RentalPayment` 
ADD CONSTRAINT FK_rentalPaymentRental
FOREIGN KEY (`rentalId`) REFERENCES `Rental`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `RentalPayment` 
ADD CONSTRAINT FK_rentalPaymentPayment
FOREIGN KEY (`paymentId`) REFERENCES `Payment`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;



CREATE TABLE IF NOT EXISTS `ProductReturn` (
  `storeId` int(11) NOT NULL,
  `saleId` int(11) NOT NULL,
  `inventoryItemId` int(11) NOT NULL,
  `reason` varchar(200),
  `condition` tinyint(2) NOT NULL,
  `refundAmount` decimal(10,2) NOT NULL,
  `returnData` varchar(200)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `ProductReturn` 
ADD CONSTRAINT FK_productReturnStore
FOREIGN KEY (`storeId`) REFERENCES `Store`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `ProductReturn` 
ADD CONSTRAINT FK_productReturnSale
FOREIGN KEY (`saleId`) REFERENCES `Sale`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `ProductReturn` 
ADD CONSTRAINT FK_productReturnItem
FOREIGN KEY (`inventoryItemId`) REFERENCES `InventoryItem`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `Role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

CREATE TABLE IF NOT EXISTS `Sale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventoryItemId` int(11) NOT NULL,
  `personId` int(11) NOT NULL,
  `purchaseDate` varchar(20) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Sale` 
ADD CONSTRAINT FK_saleItem
FOREIGN KEY (`inventoryItemId`) REFERENCES `InventoryItem`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `Sale` 
ADD CONSTRAINT FK_salePerson
FOREIGN KEY (`personId`) REFERENCES `Person`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `SalePayment` (
  `saleId` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `SalePayment` 
ADD CONSTRAINT FK_salePaymentSale
FOREIGN KEY (`saleId`) REFERENCES `Sale`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `SalePayment` 
ADD CONSTRAINT FK_salePaymentPayment
FOREIGN KEY (`paymentId`) REFERENCES `Payment`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS `Store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addressId` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100),
  `website` varchar(300),
  `tax` decimal(10,2) NOT NULL,
  `mainContactId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

ALTER TABLE `Store` 
ADD CONSTRAINT FK_storeAddress
FOREIGN KEY (`addressId`) REFERENCES `Address`(`id`) 
ON UPDATE CASCADE
ON DELETE CASCADE;


