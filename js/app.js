{

  var gryphonModule = angular.module('gryphonApp', []);

  var gryphonPrefix = '/gryphon.com/';

  gryphonModule.controller('GryphonController', [
    function () {
      var self = this;
      self.name = "Gryphon Games";
    }
  ]);

  gryphonModule.directive('addressWidget', [
    function () {
      return {
        templateUrl: '#!' + gryphonPrefix + 'html/address-form.html',
        restrict: 'E'
      };
    }
  ]);
}